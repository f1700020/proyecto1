@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- About -->
    <section class="about pt-100">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mb-20 scrollflow -slide-bottom -opacity">
                    <h6 class="small-title">NOSOTROS</h6>
                    <h4 class="title">¿QUIÉNES SOMOS?</h4>
                    <p class="xs-just">ANIMATIOMX, es una empresa especializada en la creación de solución de software empresarial como:</p><br>
                    <p class="xs-just"></p>
                    <ul>
                        <li> Animación 3D y 2D </li>
                        <li> Aplicaciones Móviles </li>
                        <li> Sitios Web </li>
                        <li> Marketing Digital </li>
                        <li> Sistemas Web (ERP y CRM) </li>
                        <li> Realidad Aumentada </li>
                        <li> Recorridos Virtuales </li>
                            
                        
                    </ul>
                    <p> La constante actualización de nuestros colaboradores, su profesionalismo y la atención 
                        personalizada a nuestros clientes nos han valido la confianza de ellos.
                    </p>
                    <p>Colaborar con su proyecto empresarial es nuestra razón de ser.</p>
                </div>
                <div class="col-md-4 mb-20 image">
                    <div class="img scrollflow -slide-left -opacity">
                        <a class="vid" href="https://www.youtube.com/watch?v=UrkXWG9WgC8">
                            <span class="vid-togo-button"><i class="ti-control-play"></i></span>
                        </a>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="yearimg xs-center8 scrollflow -slide-left -opacity">
                        <div class="numb">8</div>
                    </div>
                    <div class="year scrollflow -slide-bottom -opacity">
                        <h4 class="title xs-center" style="font-size:45px;color:#F3CC23;">AÑOS DE EXPERIENCIA CON</h4>
                    <h6 class="small-title text-center" style="color: white;font-size: 25px;">IDEAS EN MOVIMIENTO</h6>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- Our Team -->
    <section class="team mt-80 pb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-20 text-center scrollflow -slide-bottom -opacity">
                    <h6 class="small-title">MIEMBROS DE</h6>
                    <h4 class="title">NUESTRO EQUIPO</h4>
                </div>
                <div class="col-md-10 offset-md-1 scrollflow -slide-top -opacity">
                    <div class="owl-carousel owl-theme text-center">
                        <div class="item">
                            <div class="team-img"> <img loading="lazy" src="{{asset('img/equipo/arbid.png')}}" alt="Animatiomx">
                                <div class="info valign">
                                    <div class="text-center full-width">
                                        <div class="social">
                                            <a href="#" class="icon"> <i class="ti-facebook"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-twitter"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-linkedin"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6>Arbid Hueppa</h6> <span>CEO</span>
                        </div>
                        <div class="item">
                            <div class="team-img"> <img loading="lazy" src="{{asset('img/equipo/moni.jpg')}}" alt="Animatiomx">
                                <div class="info valign">
                                    <div class="text-center full-width">
                                        <div class="social">
                                            <a href="#" class="icon"> <i class="ti-facebook"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-twitter"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-linkedin"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6>Monica Zamora</h6> <span>Direccion Administrativa</span>
                        </div>
                        <div class="item">
                            <div class="team-img"> <img loading="lazy" src="{{asset('img/equipo/chemi.jpg')}}" alt="Animatiomx">
                                <div class="info valign">
                                    <div class="text-center full-width">
                                        <div class="social">
                                            <a href="#" class="icon"> <i class="ti-facebook"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-twitter"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-linkedin"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6>Jose Miguel</h6> <span>Desarrollador web y movil</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 offset-md-1 scrollflow -slide-top -opacity">
                    <div class="owl-carousel owl-theme text-center">
                        <div class="item">
                            <div class="team-img"> <img loading="lazy" src="{{asset('img/equipo/sergio.jpg')}}" alt="Animatiomx">
                                <div class="info valign">
                                    <div class="text-center full-width">
                                        <div class="social">
                                            <a href="#" class="icon"> <i class="ti-facebook"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-twitter"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-linkedin"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6>Sergio Peña</h6>Desarrollador web y movil<span></span>
                        </div>
                        <div class="item">
                            <div class="team-img"> <img loading="lazy" src="{{asset('img/equipo/sergio.jpg')}}" alt="Animatiomx">
                                <div class="info valign">
                                    <div class="text-center full-width">
                                        <div class="social">
                                            <a href="#" class="icon"> <i class="ti-facebook"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-twitter"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-linkedin"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6>Samuel Santos</h6> <span>Desarrollador web y movil</span>
                        </div>
                        <div class="item">
                            <div class="team-img"> <img loading="lazy" src="{{asset('img/equipo/chemi.jpg')}}" alt="Animatiomx">
                                <div class="info valign">
                                    <div class="text-center full-width">
                                        <div class="social">
                                            <a href="#" class="icon"> <i class="ti-facebook"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-twitter"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-linkedin"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6>Ulises Suarez</h6> <span>Desarrollador web y movil</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 offset-md-1 scrollflow -slide-top -opacity">
                    <div class="owl-carousel owl-theme text-center">
                        <div class="item">
                            <div class="team-img"> <img loading="lazy" src="{{asset('img/equipo/arbid.png')}}" alt="Animatiomx">
                                <div class="info valign">
                                    <div class="text-center full-width">
                                        <div class="social">
                                            <a href="#" class="icon"> <i class="ti-facebook"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-twitter"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-linkedin"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6>Luis vadillo</h6>Post Produccion 3D<span></span>
                        </div>
                        <div class="item">
                            <div class="team-img"> <img loading="lazy" src="{{asset('img/equipo/clau.jpg')}}" alt="Animatiomx">
                                <div class="info valign">
                                    <div class="text-center full-width">
                                        <div class="social">
                                            <a href="#" class="icon"> <i class="ti-facebook"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-twitter"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-linkedin"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6>Alejandra Yosahandi</h6> <span>Modelador y animador 3D</span>
                        </div>
                        <div class="item">
                            <div class="team-img"> <img loading="lazy" src="{{asset('img/equipo/chemi.jpg')}}" alt="Animatiomx">
                                <div class="info valign">
                                    <div class="text-center full-width">
                                        <div class="social">
                                            <a href="#" class="icon"> <i class="ti-facebook"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-twitter"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-linkedin"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6>Rolando Munguia</h6> <span>Modelador y animador 3D</span>
                        </div>
                        <div class="item">
                            <div class="team-img"> <img loading="lazy" src="{{asset('img/equipo/sergio.jpg')}}" alt="Animatiomx">
                                <div class="info valign">
                                    <div class="text-center full-width">
                                        <div class="social">
                                            <a href="#" class="icon"> <i class="ti-facebook"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-twitter"></i> </a>
                                            <a href="#" class="icon"> <i class="ti-linkedin"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h6>Eliecer Cumana</h6> <span>Audio</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection