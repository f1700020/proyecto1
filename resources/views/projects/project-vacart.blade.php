@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 pt-4">
                    <img src="img/detalle-proyecto/vacart1.jpg" alt="RealidadAumentada">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/vacart2.png" alt="RealidadAumentada">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/vacart3.png" alt="RealidadAumentada">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/vacart4.png" alt="RealidadAumentada">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/vacart5.png" alt="RealidadAumentada">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">Vacart Realidad Aumentada</h4>
                    <p class="xs-just">Se le desarrollo una App compatible con Android e IOS mediante la cual sus clientes, con sólo enfocar al logotipo de la empresa reproducen su video institucional, descargan el  catálogo de productos, acceden a las redes sociales o bien pueden comunicarse con la empresa o enviarles un mensaje vía whats apps.</p>
                </div>
                <!---h1----->
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">Sorprende a tus clientes </h4>
                    <p class="xs-just">Permite que interactúen con tus productos desde sus dispositivos móviles.</p>
                    <ul >
                        <li style="list-style-type: square"> Realidad aumentada con video </li>
                        <li style="list-style-type: square"> Realidad aumentada Interactiva   </li>
                        <li style="list-style-type: square"> Realidad aumentada con Modelado 3D  </li>
                        <li style="list-style-type: square"> Realidad aumentada con Animación </li>
                     </ul>
                    
                </div>
                


                <!---End - h1----->
                <div class="col-md-3 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp; </span><span style="font-weight:100;">Realidad Aumentada</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp; </span><span style="font-weight:100;">2019</span><br>
                    <span style="font-weight:300;">CLIENTE:&nbsp; </span><span style="font-weight:100;">Grupo Comercial Vacart</span>
                </div>
            </div>
            <!-- Formulario -->
            <div class="row mt-5">
                <div class="col-md-12 text-center scrollflow -slide-bottom -opacity">
                    
                    <h4 class="title">REALIDAD AUMENTADA </h4>
                </div>
                <div class="col-md-6 offset-md-3">

                    <form id="Form" method="POST" action="{{url('contacto')}}" class="form scrollflow -slide-bottom -opacity">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('name')}}" id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                                        {{$errors->first('name')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('email')}}" id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        {{$errors->first('email')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('phone')}}" id="form_phone" type="text" name="phone"
                                        maxlength="10" placeholder="Número celular " required="required" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        {{$errors->first('phone')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('asunto')}}" id="form_asunto" type="text" name="asunto" placeholder="Asunto" required="required">
                                        {{$errors->first('asunto')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">Producto  </option>
                                            <option value="Realidad aumentada con video ">
                                                Realidad aumentada con video  
                                            </option>
                                            <option value="Realidad aumentada Interactiva ">
                                                Realidad aumentada Interactiva   
                                            </option>
                                            <option value="Marketing Content">
                                                Marketing Content  
                                            </option>
                                            <option value="Realidad aumentada con Modelado 3D: 10 modelados ">
                                                Realidad aumentada con Modelado 3D: 10 modelados  
                                            </option>
                                            <option value="Realidad aumentada con Animación: 3 interacciones y 10 objetivos  ">
                                                Realidad aumentada con Animación: 3 interacciones y 10 objetivos  
                                            </option>
                                            <option value="Campos comunes del formulario: Nombre, email, número celular ">
                                                Campos comunes del formulario: Nombre, email, número celular   
                                            </option>
                                            
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Su empresa cuenta con logotipo, colores y tipografía? </option>
                                            <option value="a)Sí">
                                            
                                                a)Sí
                                            </option>
                                            <option value="b)No">
                                            
                                                b)No  
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                      
                                
                                </div>
                               
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn"><span>ENVIAR<i class="ti-arrow-right"></i></span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @if(session()->has('alert-success'))
    <div class="alert alert-success">
     {{ session()->get('alert-success') }}
    </div>
@endif
                </div>
            </div>
         


            <!-- End Formulario -->
           
       
            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">REALIDAD AUMENTADA</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection