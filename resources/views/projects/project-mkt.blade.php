@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 pt-4">
                    <img src="img/detalle-proyecto/mkt5.jpg" alt="MarketingDigital">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/mkt1.jpg" alt="MarketingDigital">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/mkt2.jpg" alt="MarketingDigital">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/mkt3.jpg" alt="MarketingDigital">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/mkt4.jpg" alt="MarketingDigital">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">Animatiomx</h4>
                    <p class="xs-just">Con el objetivo de aumentar el número de usuarios que visitan nuestros sitios web,  incrementar la captación de Leads y medir el desempeño de los canales de tráfico web se implementaron diversas estrategias de Marketing Digital. En primer lugar se trabajó una estrategia de optimización SEO para todos los sitios web y plataformas de Animatiomx.</p><br>

                    <p class="xs-just">Posteriormente se realizaron campañas de publicidad Adwords e Emaling para mejorar su Brand awareness (Reconocimiento de marca). Simultáneamente se instauró una Content Strategy para sus Blogs y Redes Sociales.</p>
                </div>
                <div class="col-md-3 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp; </span><span style="font-weight:100;">Marketing Digital</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp; </span><span style="font-weight:100;">2019-2020</span><br>
                    <!--<span style="font-weight:300;">CLIENTE:&nbsp; </span><span style="font-weight:100;">Animatiomx</span>-->
                </div>
            </div>
            <!---h1---->
            <div class="row">
                <div class="col-md-9">
                    <h6 class="title pt-3 xs-center" style="color:#F3CC23">Alcanza tus objetivos</h6>
                    <p class="xs-just">Posicionamos tu sitio web mediante estrategias de publicidad digital. </p>

                    <p class="xs-just">Solicita tu cotización ahora</p>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">ADWORDS</h4>
                    <p class="xs-just">Formulamos tu estrategia de posicionamiento web para ubicar tus anuncios Adword en los primeros lugares de búsqueda. Creamos campañas Search, Display, Shopping, Video y Gmail en función a tus objetivos empresariales.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">INBOUND MARKETING</h4>
                    <p class="xs-just">Diseñamos estrategias de captación de leads o prospectos, desde tu sitio web con base en tus objetivos. Creamos anuncios que favorecen la prospectación y motivan a tus clientes a recibir información adicional de tus productos o bien acceder a promociones.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">SEO</h4>
                    <p class="xs-just">Posiciona de manera orgánica a tu sitio web, mediante una optimización de tu sitio web y la implementación de palabras claves de búsqueda.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">MARKET CONTENT: BLOG</h4>
                    <p class="xs-just">Ofrece información relevante para la web y atrae usuarios a tu sitio. Eso se traduce en una mejora de calificación de tu sitio web ante los motores de búsqueda y con ello se lleva tráfico hacia el mismo.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">SOCIAL MEDIA</h4>
                    <p class="xs-just">Las redes sociales son espacios de búsqueda y recomendación de proveedores, productos y servicios. Aprovéchalos para atraer prospectos. Animatiomx te asesora. Pregunta por nuestros paquetes de social management para interactuar de manera profesional con tu target y atraer leads a tu empresa.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">ANALYTICS</h4>
                    <p class="xs-just">Obtenemos métricas de tráfico de tu sitio web y con ellas evaluamos tus campañas adword o bien identificamos el posicionamiento de tu sitio web: usuarios, lugares desde donde te visitan, horarios de navegación y las páginas que más acceden en tu sitio web entre otros indicadores.</p>
                </div>
            </div>
            <!-- Formulario -->
            <div class="row mt-5">
                <div class="col-md-12 text-center scrollflow -slide-bottom -opacity">
                    
                    <h4 class="title">MARKETING DIGITAL </h4>
                </div>
                <div class="col-md-6 offset-md-3">

                    <form id="Form" method="POST" action="{{url('contacto')}}" class="form scrollflow -slide-bottom -opacity">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('name')}}" id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                                        {{$errors->first('name')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('email')}}" id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        {{$errors->first('email')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('phone')}}" id="form_phone" type="text" name="phone"
                                        maxlength="10" placeholder="Número celular " required="required" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        {{$errors->first('phone')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('asunto')}}" id="form_asunto" type="text" name="asunto" placeholder="Asunto" required="required">
                                        {{$errors->first('asunto')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">Producto  </option>
                                            <option value="Adwords">
                                                Adwords 
                                            </option>
                                            <option value="Inbound Marketing">
                                                Inbound Marketing  
                                            </option>
                                            <option value="Marketing Content">
                                                Marketing Content  
                                            </option>
                                            <option value="Optimización SEO">
                                                Optimización SEO 
                                            </option>
                                            <option value="Social Media Management">
                                                Social Media Management 
                                            </option>
                                            <option value="Analytics">
                                                Analytics  
                                            </option>
                                            <option value="Mailing">
                                                Mailing 
                                            </option>
                                            <option value="Ads Facebook">
                                                Ads Facebook 
                                            </option>
                                            <option value="Ads Instagram">
                                                Ads Instagram  
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Su empresa cuenta con sitio web?</option>
                                            <option value="a)Sí">
                                            
                                                a)Sí
                                            </option>
                                            <option value="b)No">
                                            
                                                b)No  
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Su empresa cuenta con logotipo, colores y tipografía?</option>
                                            <option value="a)Sí">
                                            
                                                a)Sí
                                            </option>
                                            <option value="b)No">
                                            
                                                b)No  
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                
                                </div>
                               
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn"><span>ENVIAR<i class="ti-arrow-right"></i></span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @if(session()->has('alert-success'))
    <div class="alert alert-success">
     {{ session()->get('alert-success') }}
    </div>
@endif
                </div>
            </div>
         


            <!-- End Formulario -->
            
        
    


            
            <!---h1---->
            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">MARKETING DIGITAL</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection