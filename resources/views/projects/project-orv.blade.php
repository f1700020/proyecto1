@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 pt-4">
                    <img src="public/img/detalle-proyecto/app-orvity1.png" alt="Aplicacion-orvity">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="public/img/detalle-proyecto/app-orvity2.png" alt="Aplicacion-orvity">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="public/img/detalle-proyecto/app-orvity3.png" alt="Aplicacion-orvity">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="public/img/detalle-proyecto/app-orvity4.png" alt="Aplicacion-orvity">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="public/img/detalle-proyecto/app-orvity2.png" alt="Aplicacion-orvity">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">ORVITY</h4>
                    <p class="xs-just">Se desarolló una aplicación móvil con la integración de nuevas tecnologías donde se busca complementar el uso de diversas plataformas y redes sociales para la gestión de negocios. Además se permitirá la conexión entre empresas para crear acuerdos comerciales y hacer más eficientes los acuerdos existentes.</p><br>

                    <p class="xs-just">Orvity es una plataforma de gestión organizacional que hace más eficientes los
                    procesos, tantos internos como externos de las empresas, mediante herramientas que facilitan la operación constante de los usuarios. Su principal objetivo es brindar a las empresas la oportunidad de entrar al mercado competitivo, mediante herramientas reunidas en un sistema integral, que son necesarias para mejorar los procesos productivos, tanto de admistración como de operación empresarial.</p>
                </div>
                <div class="col-md-3 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp; </span><span style="font-weight:100;">App móvil</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp; </span><span style="font-weight:100;">2019-2020</span><br>
                    <span style="font-weight:300;">CLIENTE:&nbsp; </span><span style="font-weight:100;">Orvity Corp</span>
                </div>
            </div>
            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">APLICACIONES MÓVILES</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection