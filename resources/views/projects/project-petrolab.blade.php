@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 pt-4">
                    <img src="img/detalle-proyecto/petro1.jpg" alt="Recorrido-virtual-3D">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/petro2.jpg" alt="Recorrido-virtual-3D">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/petro3.jpg" alt="Recorrido-virtual-3D">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/petro4.jpg" alt="Recorrido-virtual-3D">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/petro5.jpg" alt="Recorrido-virtual-3D">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">PETROADLAB</h4>
                    <p class="xs-just">Se creó el tour virtual de los laboratorios de la empresa Petroaadlab para que sus clientes conozcan de manera interactiva sus instalaciones. Se realizó un modelado 3D de las instalaciones con vistas 360° y 180° aportando la sensación de encontrarse en ese lugar.</p><br>

                    <p class="xs-just">PETROAADLAB es un laboratorio reconocido para realizar análisis con acreditación ante la EMA y aprobación de la CRE. Ofrece servicios especializados de toma de muestra y análisis de combustible.</p>
                </div>
                <div class="col-md-3 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp; </span><span style="font-weight:100;">Recorridos Virtuales</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp; </span><span style="font-weight:100;">2017</span><br>
                    <span style="font-weight:300;">CLIENTE:&nbsp; </span><span style="font-weight:100;">Petroadlab</span>
                </div>
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">En las manos de los clientes </h4>
                    <p class="xs-just">Podrán conocer de manera interactiva el interior de las instalaciones de un lugar en específico.</p><br>

                    <p class="xs-just">Solicita tu cotización</p>
                </div>
            </div>
            <!-- Formulario -->
            <div class="row mt-5">
                <div class="col-md-12 text-center scrollflow -slide-bottom -opacity">
                    
                    <h4 class="title">RECORRIDO VIRTUAL  </h4>
                </div>
                <div class="col-md-6 offset-md-3">

                    <form id="Form" method="POST" action="{{url('contacto')}}" class="form scrollflow -slide-bottom -opacity">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('name')}}" id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                                        {{$errors->first('name')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('email')}}" id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        {{$errors->first('email')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('phone')}}" id="form_phone" type="text" name="phone"
                                        maxlength="10" placeholder="Número celular " required="required" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        {{$errors->first('phone')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('asunto')}}" id="form_asunto" type="text" name="asunto" placeholder="Asunto" required="required">
                                        {{$errors->first('asunto')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">Producto  </option>
                                            <option value="Recorrido virtual 3D 2 plantas con 4 espacios  ">
                                                Recorrido virtual 3D 2 plantas con 4 espacios  
                                            </option>
                                            
                                            
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Su empresa cuenta con logotipo, colores y tipografía?  </option>
                                            <option value="a)Sí">
                                            
                                                a)Sí
                                            </option>
                                            <option value="b)No">
                                            
                                                b)No  
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                      
                                
                                </div>
                               
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn"><span>ENVIAR<i class="ti-arrow-right"></i></span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @if(session()->has('alert-success'))
    <div class="alert alert-success">
     {{ session()->get('alert-success') }}
    </div>
@endif
                </div>
            </div>
         


            <!-- End Formulario -->
            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">RECORRIDOS Virtuales</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
        
    </section>
</div>
@endsection
