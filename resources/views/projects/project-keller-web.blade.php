@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 pt-4">
                    <img src="img/detalle-proyecto/spot1.jpg" alt="SpotPublicitario">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/spot3.jpg" alt="SpotPublicitario">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/spot4.jpg" alt="SpotPublicitario">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/spot5.jpg" alt="SpotPublicitario">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/spot3.jpg" alt="SpotPublicitario">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">Sistema Inteligente Centinela</h4>
                    <p class="xs-just">Creación de spot publicitario para presentar las funcionalidades y las ventajas de la App Centinela a sus usuarios. Consiste en una Animación 2D Motion Graphics con una duración aproximada de 1:38 minutos. </p>
                </div>
                <div class="col-md-3 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp; </span><span style="font-weight:100;">Spot Publicitario</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp; </span><span style="font-weight:100;">2020</span><br>
                    <span style="font-weight:300;">CLIENTE:&nbsp; </span><span style="font-weight:100;">Keller</span>
                </div>
            </div>
            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">SPOT PUBLICITARIO</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection