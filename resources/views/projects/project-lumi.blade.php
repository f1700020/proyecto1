@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 pt-4">
                    <img src="img/detalle-proyecto/app-orvity1.png" alt="SistemaWeb">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/app-orvity2.png" alt="SistemaWeb">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/app-orvity3.png" alt="SistemaWeb">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/app-orvity4.png" alt="SistemaWeb">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/app-orvity2.png" alt="SistemaWeb">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">LUMI</h4>
                    <p class="xs-just">Diseño de una Plataforma Web para gestionar la atención de Leads o prospectos y medir el desempeño de la fuerza de venta de la empresa LUMI. Mediante esta herramienta se puede conocer en tiempo real el status de las cotizaciones presentadas, los integrantes de cada equipo de venta y las empresas que están atendiendo así como las visitas que se le han hecho en periodos de tiempos determinados.</p><br>

                    <p class="xs-just">Permite la evaluación de los indicadores de gestión de cada integrante de la fuerza de ventas.</p>
                </div>
                <div class="col-md-3 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp; </span><span style="font-weight:100;">Plataforma Web</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp; </span><span style="font-weight:100;">2020</span><br>
                    <span style="font-weight:300;">CLIENTE:&nbsp; </span><span style="font-weight:100;">LUMI Material Eléctrico</span>
                </div>
            </div>
             <!-- Formulario -->
             <div class="row mt-5">
                <div class="col-md-12 text-center scrollflow -slide-bottom -opacity">
                    
                    <h4 class="title">Plataforma LUMI</h4>
                </div>
                <div class="col-md-6 offset-md-3">

                    <form id="Form" method="POST" action="{{url('contacto')}}" class="form scrollflow -slide-bottom -opacity">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('name')}}" id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                                        {{$errors->first('name')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('email')}}" id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        {{$errors->first('email')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('phone')}}" id="form_phone" type="text" name="phone"
                                        maxlength="10" placeholder="Teléfono" required="required" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        {{$errors->first('phone')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('asunto')}}" id="form_asunto" type="text" name="asunto" placeholder="Asunto" required="required">
                                        {{$errors->first('asunto')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">Horario disponible para agendar una reunión </option>
                                            <option value="09:00 ">
                                                09:00 
                                            </option>
                                            <option value="10:00 ">
                                                10:00 
                                            </option>
                                            <option value="11:00 ">
                                                11:00 
                                            </option>
                                            <option value="12:00">
                                                12:00
                                            </option>
                                            <option value="01:00">
                                                01:00
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">Presupuesto destinado para el proyecto de Sistema Web  </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">Área que se beneficiara con el sistema web   </option>
                                            
                                        
                                        <option value="Ventas ">
                                            Ventas 
                                        </option>
                                        
                                        <option value="Operación">
                                            Operación
                                        </option>
                                        
                                        <option value="Administrativa ">
                                            Administrativa 
                                        </option>
                                        
                                        <option value="Recursos Humanos ">
                                            Recursos Humanos 
                                        </option>
                                        
                                        <option value="Almacén ">
                                            Almacén 
                                        </option>

                                        <option value="Toda la empresa  ">
                                            Toda la empresa  
                                        </option>
                                       </select>
                                        
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">Giro de su empresa </option>
                                            
                                        
                                        <option value="Comercial ">
                                            Comercial  
                                        </option>
                                        
                                        <option value="Industrial ">
                                            Industrial 
                                        </option>
                                        
                                        <option value="Servicios">
                                            Servicios 
                                        </option>
                                        </select>
                                        
                                 
                                        
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">Tamaño de su empresa  </option>
                                            
                                        
                                        <option value="De 0 a 5 empleados">
                                            De 0 a 5 empleados   
                                        </option>
                                        
                                        <option value="Hasta 10 empleados">
                                            Hasta 10 empleados 
                                        </option>
                                        
                                        <option value="De 11 a 30 empleados">
                                            De 11 a 30 empleados 
                                        </option>
                                        <option value="De 31 a 50 empleados ">
                                            De 31 a 50 empleados 
                                        </option>
                                        <option value="De 51 a 100 empleados ">
                                            De 51 a 100 empleados  
                                        </option>
                                        <option value="Más de 100 empleados ">
                                            Más de 100 empleados  
                                        </option>
                                       </select>
                                 
                                        
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                               
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn"><span>ENVIAR<i class="ti-arrow-right"></i></span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @if(session()->has('alert-success'))
    <div class="alert alert-success">
     {{ session()->get('alert-success') }}
    </div>
@endif
                </div>
            </div>
         


            <!-- End Formulario -->
            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">PLATAFORMA WEB</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection