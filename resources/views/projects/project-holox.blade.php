
@extends('layouts.app')

@section('content')

<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 pt-4">
                    <img src="img/detalle-proyecto/holox1.jpg" alt="Blog-Web">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/holox2.jpg" alt="Blog-Web">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/holox3.jpg" alt="Blog-Web">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/holox4.jpg" alt="Blog-Web">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/holox5.jpg" alt="Blog-Web">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">Holox - Guru</h4>
                    <p class="xs-just">Creación de un blog autoadministrable para que los clientes de la empresa Holox accedan a artículos psicoterapéuticos y holísticos especializados que  permiten a sus usuarios tener un adecuado desarrollo físico, mental, energético y espiritual El administrador del blog establece las categoría y etiquetas de cada artículo. Además cuenta con botones Messenger, Whats App y de llamadas.</p>
                </div>
                <div class="col-md-3 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp; </span><span style="font-weight:100;">Blog</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp; </span><span style="font-weight:100;">2020</span><br>
                    <span style="font-weight:300;">CLIENTE:&nbsp; </span><span style="font-weight:100;">Holox</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">Blog</h4>
                    <p class="xs-just">Módulo administrativo, Dominio, Hosting, cuentas de correo, Optimización SEO, Código Google Analytics para monitoreo de rendimiento.</p>
                    <p class="xs-just">Un blog es una herramienta que puede ser muy útil para alcanzar objetivos corporativos y entablar un diálogo con la audiencia.</p>
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 g-4">
                <div class="col">
            
            <div class="col">
                
                <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                <div class="card-header" style="color: #F3CC23;">USOS   </div>
                <div class="card-body">
                    <ul >
                        <li style="list-style-type: square"> Compartir contenido    </li>
                        <li style="list-style-type: square"> Almacenar información    </li>
                        <li style="list-style-type: square"> Publicar artículos   </li>
                        <li style="list-style-type: square"> Información complementaria de los servicios o productos de la empresa   </li>
                        
                            
                        
                    </ul>
                    <div class="card-header" style="color: #F3CC23;">Tiempo de desarrollo estimado: Tres Semanas</div>
                  
                
                </div>
              </div>
            
              </div>
              
                </div>
            </div>
            @include('frontend.slider.slider')
            <!-- Formulario -->
            <div class="row mt-5">
                <div class="col-md-12 text-center scrollflow -slide-bottom -opacity">
                    
                    <h4 class="title">Holox - Guru </h4>
                </div>
                <div class="col-md-6 offset-md-3">

                    <form id="Form" method="POST" action="{{url('contacto')}}" class="form scrollflow -slide-bottom -opacity">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('name')}}" id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                                        {{$errors->first('name')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('email')}}" id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        {{$errors->first('email')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('phone')}}" id="form_phone" type="text" name="phone"
                                        maxlength="10" placeholder="Número celular " required="required" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        {{$errors->first('phone')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('asunto')}}" id="form_asunto" type="text" name="asunto" placeholder="Asunto" required="required">
                                        {{$errors->first('asunto')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Qué tipo de presencia web requiere?  </option>
                                            <option value="Landing Page">
                                                Landing Page 
                                            </option>
                                            <option value="One Page">
                                                One Page   
                                            </option>
                                            <option value="Sitio Web">
                                                Sitio Web   
                                            </option>
                                            <option value="Blog">
                                                Blog 
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Su empresa cuenta con logotipo, colores y tipografía?</option>
                                            <option value="a)Sí">
                                            
                                                a)Sí
                                            </option>
                                            <option value="b)No">
                                            
                                                b)No  
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Requiere que se incorporen Botones Whats App?</option>
                                            <option value="a)Sí">
                                            
                                                a)Sí
                                            </option>
                                            <option value="b)No">
                                            
                                                b)No  
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Necesita la Opción de llamada desde presencia web?</option>
                                            <option value="a)Sí">
                                            
                                                a)Sí
                                            </option>
                                            <option value="b)No">
                                            
                                                b)No  
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Requiere panel administrativo para realizar cambios con regularidad a su sitio web?</option>
                                            <option value="a)Sí">
                                            
                                                a)Sí
                                            </option>
                                            <option value="b)No">
                                            
                                                b)No  
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                
                                </div>
                               
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn"><span>ENVIAR<i class="ti-arrow-right"></i></span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @if(session()->has('alert-success'))
    <div class="alert alert-success">
     {{ session()->get('alert-success') }}
    </div>
@endif
                </div>
            </div>
         


            <!-- End Formulario -->
            
        

            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">BLOG'S</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection