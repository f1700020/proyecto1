@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 pt-4">
                    <img src="img/detalle-proyecto/sustent1.jpg" alt="PaginaWeb">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/sustent2.jpg" alt="PaginaWeb">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/sustent3.jpg" alt="PaginaWeb">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/sustent4.jpg" alt="PaginaWeb">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/sustent5.jpg" alt="PaginaWeb">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">Sustenthabit</h4>
                    <p class="xs-just">Diseño de un sitio web para un despacho de arquitectos. Enfocada para que sus clientes conozcan sus servicios de forma concreta y sencilla. Muestra el portafolio actualizado y cuenta con formulario de contacto, link para acceso a redes sociales y WhatsApp</p>
                </div>
                <div class="col-md-3 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp; </span><span style="font-weight:100;">Página Web</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp; </span><span style="font-weight:100;">2020</span><br>
                    <span style="font-weight:300;">CLIENTE:&nbsp; </span><span style="font-weight:100;">Sustenthabit</span>
                </div>
            </div>
            <!---sistema web---->
            
            <h4 class="title pt-3 xs-center" style="color:#F3CC23">SISTEMAS WEB (ERPS)</h4>
             <p>Digitaliza a tu empresa </p>
             <p>Desarrollamos sistemas web para un área o para toda tu empresa. <br>

                Solicita una consultoría sin costo  </p>

                <!-- Inicia la cotización -->
             <div class="row row-cols-1 row-cols-md-3 g-4">
                <div class="col">
                  
                <!--1h--->
                <div class="card text-white bg-dark mb-3">
                <div class="item mb-50 scrollflow -pop -opacity" style="width:100%;">
                <div class="card-header" style="color: #F3CC23;">   ERP  </div>
                <div class="card-body">
                  
                  <p class="card-text" style="text-align: justify;">Consiste en el uso de sistemas web para llevar a cabo la operación de la empresa. Incluye la administración, logística, suministros, inventarios, ventas entre otras funcionalidades. <br>

                    Algunos sistemas web son trajes a la medida mientras que otros son sistemas prediseñados que se ajustan a la operación por sector o ramo al que pertenece la empresa.  </p>
                 </div>
                </div>
            
                </div>
            </div>
              <!--2h--->
              <div class="col">
                
                <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                    <div class="item mb-50 scrollflow -pop -opacity" style="width:100%;">
                <div class="card-header" style="color: #F3CC23;">CRM   </div>
                <div class="card-body">
                  
                <p class="card-text" style="text-align: justify;">Es un sistema web mediante la cual se centraliza en una sola base de datos todas las interacciones entre una empresa y sus clientes.    </p>
                </div>
                </div>
                </div>
                </div>
             
       
            </div>
            <!---End sistema web---->
            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">PAGINAS WEB</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection