@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 pt-4">
                    <img src="img/detalle-proyecto/kellerapp4.jpg" alt="DesarroloDeApps">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/kellerapp1.jpg" alt="DesarroloDeApps">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/kellerapp2.jpg" alt="DesarroloDeApps">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/kellerapp3.jpg" alt="DesarroloDeApps">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/kellerapp5.jpg" alt="DesarroloDeApps">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">Sistema Inteligente Centinela</h4>
                    <p class="xs-just" style="text-align: justify;">Creación de una App para administrar y gestionar a los grupos de seguridad patrimonial. Permite la programación de las tareas y rondas de los guardias de seguridad. Identifica a aquellos que no realizan sus asignaciones y en caso de emergencia, activan un botón SOS para iniciar la grabación de las incidencias y se pueda utilizar en una investigación posterior, además de que permite ayudarlo en tiempo real.</p><br>

                    <p class="xs-just" style="text-align: justify;">Es una herramienta útil para monitorear los indicadores de gestión de cada guardia de seguridad de su empresa.</p>
                </div>
                <div class="col-md-3 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp; </span><span style="font-weight:100;">App móvil</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp; </span><span style="font-weight:100;">2020</span><br>
                    <span style="font-weight:300;">CLIENTE:&nbsp; </span><span style="font-weight:100;">Keller</span>
                </div>
            </div>
            <!---App--->
            <h5 class="title" style="color: #F3CC23;">Desarrollamos aplicaciones móviles para PYMES y emprendimientos. </h5>
            <p style="text-align: justify;">Nos especializamos en el desarrollo de Apps móviles para empresas y Stars Up. Creamos aplicaciones en plataformas iOS, Android e Hibridas. </p>
            
            <!---Cotiza tu proyecto--->
            <div class="row row-cols-1 row-cols-md-3 g-4">
                <div class="col">
            
            <div class="col">
                
                <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                <div class="card-header" style="color: #F3CC23;">Cotiza tu proyecto.   </div>
                <div class="card-body">
                    <ul >
                        <li style="list-style-type: square"> Funcionalidades   </li>
                        <li style="list-style-type: square"> Juegos   </li>
                        <li style="list-style-type: square"> Geolocalización   </li>
                        <li style="list-style-type: square"> Chats   </li>
                        <li style="list-style-type: square"> Notificaciones    </li>
                        <li style="list-style-type: square"> Inicio de sesión     </li>
                        <li style="list-style-type: square"> Panel Administrativo    </li>
                        <li style="list-style-type: square"> Interacción con otros sistemas    </li>
                        <li style="list-style-type: square"> Ecommerce    </li>
                        
                            
                        
                    </ul>
                  
                
                </div>
              </div>
            
              </div>
              
                </div>
            </div>
        </div>
        

            <!---App--->
            <!-- Formulario -->
            <div class="row mt-5">
                <div class="col-md-12 text-center scrollflow -slide-bottom -opacity">
                    
                    <h4 class="title">Aplicaciones Móviles </h4>
                </div>
                <div class="col-md-6 offset-md-3">

                    <form id="Form" method="POST" action="{{url('contacto')}}" class="form scrollflow -slide-bottom -opacity">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('name')}}" id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                                        {{$errors->first('name')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('email')}}" id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        {{$errors->first('email')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('phone')}}" id="form_phone" type="text" name="phone"
                                        maxlength="10" placeholder="Número celular " required="required" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        {{$errors->first('phone')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('asunto')}}" id="form_asunto" type="text" name="asunto" placeholder="Asunto" required="required">
                                        {{$errors->first('asunto')}}
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Su App Requiere un Panel de Administración? </option>
                                            <option value="a)Sí">
                                            
                                                a)Sí
                                            </option>
                                            <option value="b)No, solo Requiero Altas, bajas y cambios ">
                                            
                                                b)No, solo Requiero Altas, bajas y cambios   
                                            </option>
                                            <option value="a)Sí">
                                            
                                                c)No Lo Se
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Necesita que sus usuarios tengan información dependiendo de su localización?</option>
                                            <option value="a)Si, Geolocalización">
                                            
                                                a)Si, Geolocalización
                                            </option>
                                            <option value="b)No, Geolocalización">
                                            
                                                b)No, Geolocalización  
                                            </option>
                                            <option value="c)No lo se">
                                            
                                                c)No lo se
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Tendrá pagos en Línea dentro de la App?</option>
                                            <option value="a)Si, pagos en línea">
                                            
                                                a)Si, pagos en línea
                                            </option>
                                            <option value="b)No, pagos en línea">
                                            
                                                b)No, pagos en línea  
                                            </option>
                                            <option value="c)No lo se">
                                            
                                                c)No lo se
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Requiere inicio de sesión con perfil incluido?</option>
                                            <option value="a)Si, con perfil de redes sociales">
                                            
                                                a)Si, con perfil de redes sociales
                                            </option>
                                            <option value="b)Por registro de formulario">
                                            
                                                b)Por registro de formulario  
                                            </option>
                                            <option value="c)No lo se">
                                            
                                                c)No lo se
                                            </option>
                                            <option value="d)Por SMS">
                                            
                                                d)Por SMS
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Requiere que su App contenga chat?</option>
                                            <option value="a)Si, chat normal multimedia con grupos">
                                            
                                                a)Si, chat normal multimedia con grupos
                                            </option>
                                            <option value="b)Si, chat normal multimedia">
                                            
                                                b)Si, chat normal multimedia  
                                            </option>
                                            <option value="c)Si, chat normal">
                                            
                                                c)Si, chat normal
                                            </option>
                                            <option value="d)Si, chat normal con grupos">
                                            
                                                d)Si, chat normal con grupos
                                            </option>
                                            <option value="e)No requiero chat">
                                            
                                                e)No requiero chat
                                            </option>
                                            <option value="f)No lo se">
                                            
                                                f)No lo se
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" >
                                            <option value="{{old('subject')}}">¿Enviaras notificaciones personalizadas en tiempo real?</option>
                                            <option value="a)Si requiero enviar notificaciones">
                                            
                                                a)Si requiero enviar notificaciones
                                            </option>
                                            <option value="b)Si, enviar información a otro sistema">
                                            
                                                b)Si, enviar información a otro sistema  
                                            </option>
                                            <option value="c)Recibir y enviar información con otros sistemas">
                                            
                                                c)Recibir y enviar información con otros sistemas  
                                            </option>
                                            <option value="d)No lo sé">
                                            
                                                d)No lo sé 
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                               
                                
                                </div>
                               
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn"><span>ENVIAR<i class="ti-arrow-right"></i></span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @if(session()->has('alert-success'))
    <div class="alert alert-success">
     {{ session()->get('alert-success') }}
    </div>
@endif
                </div>
            </div>
         


            <!-- End Formulario -->
            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">APLICACIONES MÓVILES</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection