@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-3 pt-4">
                    <img src="img/detalle-proyecto/tem5.jpg" alt="One-Page">
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/tem1.jpg" alt="One-Page">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/tem2.jpg" alt="One-Page">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/tem3.jpg" alt="One-Page">
                        </div>
                        <div class="col-md-6 pt-4">
                            <img src="img/detalle-proyecto/tem4.jpg" alt="One-Page">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">TEM</h4>
                    <p class="xs-just">Desarrollo de una One Page para que sus prospectos y clientes puedan conocer la trayectoria y servicios que ofrece la empresa Tecnología Ecológica en Movimiento. Se le integró elementos de contacto como formulario, botón Facebook y WhatsApp.</p>
                </div>
                <div class="col-md-4 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp; </span><span style="font-weight:100;">One Oage</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp; </span><span style="font-weight:100;">2020</span><br>
                    <span style="font-weight:300;">CLIENTE:&nbsp; </span><span style="font-weight:100;">Tecnología Ecológica en Movimiento</span>
                </div>
            </div>
            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">SISTEMAS WEB</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection