@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post">
        <div class="container">
            <div class="row">
                <div class="col-md-4 pt-4">
                    <img src="img/detalle-proyecto/animacion10.jpg" alt="Animacion3D">
                </div>
                <div class="col-md-4 pt-4">
                    <img src="img/detalle-proyecto/animacion2.jpg" alt="Animacion3D">
                </div>
                <div class="col-md-4 pt-4">
                    <img src="img/detalle-proyecto/animacion3.jpg" alt="Animacion3D">
                </div>
            </div>
                
                    
            <div class="row">
                <div class="col-md-4 pt-4">
                    <img src="img/detalle-proyecto/animacion4.jpg" alt="Animacion3D">
                </div>
                <div class="col-md-4 pt-4">
                    <img src="img/detalle-proyecto/animacion5.jpg" alt="Animacion3D">
                </div>
                <div class="col-md-4 pt-4">
                    <img src="img/detalle-proyecto/animacion6.jpg" alt="Animacion3D">
                </div>
            </div>
 
            <div class="row">
                <div class="col-md-4 pt-4">
                    <img src="img/detalle-proyecto/animacion7.jpg" alt="Animacion3D">
                </div>
                <div class="col-md-4 pt-4">
                    <img src="img/detalle-proyecto/animacion8.jpg" alt="Animacion3D">
                </div>
                <div class="col-md-4 pt-4">
                    <img src="img/detalle-proyecto/animacion9.jpg" alt="Animacion3D">
                </div>
            </div>

            <div class="row">
                <div class="col-md-9">
                    <h4 class="title pt-3 xs-center" style="color:#F3CC23">La Creación</h4>
                    <p class="xs-just">Largometraje en 3D que cuenta la historia de la Creación  de acuerdo al Génesis de la biblia y desde la perspectiva de la Iglesia Adventista. Forma parte de la serie Exploración del Mundo Bíblico. Está enfocado a los feligreses de la División Interamericana Adventista.</p><br>
                    <p class="xs-just">Para su realización se recurrió al Motion Capture y se integraron distintos tipos de escenarios como océanos, bosques o valles. Además de modelados, sonorización y masterización para crear un producto cinematográfico. Tiene una duración de 16 minutos. </p><br>
                </div>
                <div class="col-md-3 pt-3">
                    <h4 class="title" style="color: #F3CC23;">INFO</h4>
                   <span style="font-weight:300;">CATEGORÍA:&nbsp;</span><span style="font-weight:100;">Animación 3D</span><br>
                    <span style="font-weight:300;">FECHA:&nbsp;</span><span style="font-weight:100;">2019-2020</span><br>
                    <span style="font-weight:300;">CLIENTE:&nbsp;</span><span style="font-weight:100;">División Interamericana Adventista</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <h5 class="title" style="color: #F3CC23;">Aprovecha al máximo el poder de la animación 3D y 2D </h5>
                </div>
             
            </div>
            <ul >
                <li style="list-style-type: square"> Transmite emociones  </li>
                <li style="list-style-type: square"> Empodera a tu público  </li>
                <li style="list-style-type: square"> Creamos contenido atractivo  </li>
                <li style="list-style-type: square"> Genera leads con tus campañas  </li>
                
                    
                
            </ul>
            <p>Potencializa tus proyectos de marketing y de comunicación estratégica para tu empresa. </p>
            <div class="row">
                <div class="col-md-9">
                    <h5 class="title" style="color: #F3CC23;"> Solicita tu cotización  </h5>
                </div>
             
            </div>
             <!-- Inicia la cotización -->
             <div class="row row-cols-1 row-cols-md-3 g-4">
                <div class="col">
                  
                <!--1h--->
                <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                <div class="item mb-50 scrollflow -pop -opacity" style="width:100%;">
                <div class="card-header" style="color: #F3CC23;">Spots publicitarios 2D stop motions  </div>
                <div class="card-body">
                  
                  <p class="card-text" style="text-align: justify;">Animación digital que combina diferentes elementos gráficos como imágenes, fotografías, títulos, colores y diseño. </p>
                 </div>
                </div>
            
                </div>
            </div>
              <!--2h--->
              <div class="col">
                
                <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                <div class="card-header" style="color: #F3CC23;">Spots publicitarios 2D motion graphics   </div>
                <div class="card-body">
                  
                <p class="card-text" style="text-align: justify;">Consiste en dotar de movimiento a objetos estáticos mediante una sucesión de imágenes fijas. Se crea la ilusión de movimiento mediante programas de edición y técnicas digitales, simulando la animación. Los elementos gráficos que se utilizan en un vídeo Motion Graphics son las figuras, fotografías e imágenes, las tipografías y letras digitales, los colores, diseños, etc.   </p>
                </div>
              </div>
            
              </div>
              <!--3h--->
              <div class="col">
                
                <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                <div class="card-header" style="color: #F3CC23;">Spots publicitarios 3D motion graphics  </div>
                <div class="card-body">
                  
                <p class="card-text" style="text-align: justify;">Anuncios que simulan la visualización tridimensional.  Contamos con un equipo de especialistas dedicados al diseño, modelado y animación de objetos y personajes en 3D.  </p>
                     </div>
                   </div>
                </div>
                 <!--4h--->
              <div class="col">
                
                <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                <div class="card-header" style="color: #F3CC23;">Cortometraje 2D Motions graphics Max 2 mins  </div>
                
                   </div>
                </div>
                   <!--5h--->
              <div class="col">
                
                <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                <div class="card-header" style="color: #F3CC23;">Cortometraje 3D Motions grapchis Máx 2 mins   </div>
                
                   </div>
                </div>
       
            </div>
            
         <!-- End cotización -->
         <!-- Formulario -->
         <br>
         <div class="col-md-12 text-center scrollflow -slide-bottom -opacity">
            
            <h4 class="title">ANIMACIÓN 3D Y 2D </h4>
        </div>
            
         <div class="row mt-5">
            
        <div class="col-md-6 offset-md-3">
         <form id="Form" method="POST" action="" class="form scrollflow -slide-bottom -opacity">
            
            
            <div class="controls">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group has-error has-danger">
                            <input value="" id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                            
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group has-error has-danger">
                            <input value="" id="form_email" type="email" name="email" placeholder="Email" required="required">
                            
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group has-error has-danger">
                            <input value="" id="form_phone" type="text" name="phone"
                            maxlength="10" placeholder="Teléfono" required="required" >
                            
                        </div>
                    </div>
                    
                    
                    <div class="col-md-15">
                        <div class="form-group">
                            <select  name="subject" id="form_subject" >
                                <option value="">Animación que requiere </option>
                                <option value="Spots publicitarios 2D stop motions">
                                    Spots publicitarios 2D stop motions 
                                </option>
                                <option value="Spots publicitarios 2D motions graphics">
                                    Spots publicitarios 2D motions graphics 
                                </option>
                                <option value="Spots publicitarios 3D motion graphics ">
                                    Spots publicitarios 3D motion graphics 
                                </option>
                                <option value="Cortometraje 2D Motions grapchis Max 2 mins">
                                    Cortometraje 2D Motions grapchis Max 2 mins 
                                </option>
                                <option value="Cortometraje 3D Motions grapchis Máx 2 mins ">
                                    Cortometraje 3D Motions grapchis Máx 2 mins 
                                </option>
                                
                            </select>
                            {{$errors->first('subject')}}
                        </div>

                    </div>

                    </div>
                   
                   
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn"><span>ENVIAR<i class="ti-arrow-right"></i></span></button>
                    </div>
                </div>
            
        </form>
    </div>
</div>
</div>


         <!-- End Formulario -->

            
            <div id="xs-p-0" class="row mx-5 my-5 px-5 py-3" style="border:1px solid white;">
                <div class="col-md-9">
                    <h5 class="mx-auto text-center">
                        CONOCE MÁS DE NUESTROS PROYECYOS
                        DE <span style="color:#F3CC23;">ANIMACIÓN 3D</span>
                    </h5>
                </div>
                <div class="col-md-3 xs-center">
                    <button type="submit" class="btn"><span>VER<i class="ti-arrow-right"></i></span></button>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection