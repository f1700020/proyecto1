<!-- Sidebar -->
    <div class="togo-fixed-sidebar togo-sidebar-left" style="border-right:1px solid white;">
        <div class="togo-header-container">
            <!-- Logo -->
            <div class="logo">
                <img src="{{asset('img/logo.png')}}" alt="Animatiomx">
            </div>
            <!-- Burger menu -->
            <div class="togo-burger-menu">
                <div class="togo-line-menu togo-line-half togo-first-line"></div>
                <div class="togo-line-menu"></div>
                <div class="togo-line-menu togo-line-half togo-last-line"></div>
            </div>
            <!-- Navigation menu -->
            <nav class="togo-menu-fixed">
                <ul>
                    <li><a href="{{url('/')}}">Inicio</a></li>
                    <li><a href="{{url('promociones')}}">PROMOCIONES</a></li>
                    <li><a href="{{url('servicios')}}">SERVICIOS</a></li>
                    <li><a href="{{url('nosotros')}}">NOSOTROS</a></li>
                    <li><a href="{{url('proyectos')}}">PROYECTOS</a></li>
                    <li><a href="{{url('contacto')}}">CONTACTO</a></li>
                    <li><a target="_blank" href="https://animatiomx.com/blog">BLOG</a></li>
                </ul>
            </nav>
            <div class="col-md-10">
                <a target="_blank" href="https://wa.me/528132806645?text=Me%20gustaría%20solicitar%20una%20cotización"><img src="{{asset('img/icono-whatsapp.png')}}" class="img mb-2" alt="Animatiomx"><br></a>
                <a target="_blank" href="https://www.facebook.com/animatiomx/"><img src="{{asset('img/icono-messenger.png')}}" class="img mb-2" alt="Animatiomx"><br></a>
                <a target="_blank" href="https://twitter.com/animatiomx?lang=es"><img src="{{asset('img/icono-twitter.png')}}" class="img mb-2" alt="Animatiomx"><br></a>
                <a target="_blank" href="https://www.instagram.com/animatiomx/?hl=es-la"><img src="{{asset('img/icono-instagram.png')}}" class="img mb-2" alt="Animatiomx"><br></a>
            </div>
            
            <div class="col-md-12 mb-2">
                    <span>ES&nbsp;</span>|<span>&nbsp;EN</span>
                </div>
            <!-- Menu social media -->
            <div class="togo-menu-social-media">
                <div class="social">
                    <a href="#"><i class="ti-facebook"></i></a>
                    <a href="#"><i class="ti-twitter"></i></a>
                    <a href="#"><i class="ti-instagram"></i></a>
                    <a href="#"><i class="ti-youtube"></i></a>
                </div>
                <div class="togo-menu-copyright">
                    <p>© 2020 Todos los derechos reservados <a href="https://animatiomx.com/">ANIMATIOMX</a></p>
                </div>
            </div>
        </div>
    </div>