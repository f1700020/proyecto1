<!DOCTYPE html>
<html lang="es">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script async>(function(w, d) { var h = d.head || d.getElementsByTagName("head")[0]; var s = d.createElement("script"); s.setAttribute("type", "text/javascript"); s.setAttribute("src", "https://app.bluecaribu.com/conversion/integration/e5eda2d1492a638c628baba66121b0b1"); h.appendChild(s); })(window, document);</script>

  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PNQD5HJ');</script>
<!-- End Google Tag Manager -->

 <!-- Anti-flicker snippet (recommended)  -->
    <!-- <style>.async-hide { opacity: 0 !important} </style> -->
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
        h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
        (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
    })(window,document.documentElement,'async-hide','dataLayer',5000,
{'GTM-WFCRVZG':true});</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140671143-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-140671143-4');
</script>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
   <!-- Basic Page Needs -->
<title>Animatiomx | Desarrollo web puebla | Desarrollo de apps puebla</title>
<meta name="google-site-verification" content="google926138e5787b9d0c.html">
<meta name="description" content="ANIMATIOMX, es una empresa especializada en la creaci��n de soluciones de software empresarial como desarrollo de sistemas web en puebla, apps en puebla, dise�0�9os de web site y realidad aumentada en puebla. Tambi��n en servicios de animaci��n 2D y 3D en puebla empleados para Spots Publicitarios puebla, Cortos y Largometrajes.">
<meta name="author" content="Animatiomx">
<meta name="keywords" content="Desarrollo de App puebla, Apps puebla, AplicacionApp, Aplicaciones Moviles puebla, Desarrollo App puebla, aplicaciones moviles en puebla">
<meta name="keywords" content="Animaci��n 3D, Dise�0�9o 3D, Animaci��n Digital, Animaci��nDigitalMonterrey">
<meta name="keywords" content="como crear paginas web puebla,Web Site puebla, Pagina Web puebla, Desarrollo Web puebla, Pagina Web Monterrey, Chatbot, EnlaceMessenger, Messenger, Apimessenger, Enlacewhatsapp, Whatsapp, Api whatsapp, Formulario de Contacto, Inbound Marketing">
<meta name="keywords" content="Spot Publicitario, Videos Animados, Anuncios de Publicidad, Animaci��n Digital, Video Marketing, Explainer Videos, Anuncios Comerciales.">
<meta name="keywords" content="ERP, ERP Pymes, ERP System, ERP Consultant, Sistemas Web,  Digitaliza tus operaciones">
<meta name="keywords" content="Recorridos Virtuales, Recorridos Virtuales para Escuelas, como hacer recorridos virtuales, recorrido virtual 3d, hacer recorridos virtuales.">
<meta name="keywords" content="Animacion digital, Aplicacion App, Web Site, Spot publicitario, Videos animados, P��gina Web, Desarrollo Web, Anuncios comerciales, app animacion 2d, Dise�0�9o 3D, Desarrollo app, Anuncios de publicidad,Aplicaciones m��viles, Adword, SEO, Realidad Aumentada, Renders Arquitectonicos, Social Media Managment.">

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.ico')}}" />

    <link rel="stylesheet" href="{{asset('css/app.css')}}"/>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700&display=swap" rel="stylesheet">
</head>
<body>
    <!-- Loader -->
    <div id="loader">
        <div class="loading">
            <div></div>
        </div>
	</div>
	<!-- Sidebar -->
	@include('layouts.menu')
	<!-- Content -->
	@yield('content')
    <!-- scrollflow -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script>
         // Testimonials owlCarousel
    $('.testimonials .owl-carousel').owlCarousel({
        loop: true
        , margin: 30
        , mouseDrag: true
        , autoplay: true
        , dots: false
        , responsiveClass: true
        , responsive: {
            0: {
                items: 1
            , }
            , 600: {
                items: 1
            }
            , 1000: {
                items: 1
            }
        }
    });

    // Team owlCarousel
    $('.team .owl-carousel').owlCarousel({
        loop: true,
        margin: 0,
        autoplay: true,
        dots: false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                autoplay:true,
            },
            600:{
                items:2,
                autoplay:true,
            },
            1000:{
                items:3,
                autoplay:false,
            }
        }
    });
    </script>
</body>
</html>