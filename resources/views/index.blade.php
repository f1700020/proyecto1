@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Header Slider -->
    <header class="header pos-re slider-fade d-flex align-items-center"
    style="background-color: black;"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <video loading="lazy" src="{{asset('img/animatiomx.mp4')}}" autoplay muted loop style="width: 100%;height: 100%;"></video>
    </header>
</div>
<!-- Politica de cookies --->
<style>
@keyframes desaparecer
{
0%		{bottom: 0px;}
80%		{bottom: 0px;}
100%		{bottom: -50px;}
}
@-webkit-keyframes desaparecer /* Safari and Chrome */
{
0%		{bottom: 0px;}
80%		{bottom: 0px;}
100%		{bottom: -50px;}
}
@keyframes aparecer
{
0%		{bottom: -38px;}
10%		{bottom: 0px;}
90%		{bottom: 0px;}
100%		{bottom: -38px;}
}
@-webkit-keyframes aparecer /* Safari and Chrome */
{
0%		{bottom: -38px;}
10%		{bottom: 0px;}
90%		{bottom: 0px;}
100%		{bottom: -38px;}
}
#cookiesms1:target {
    display: none;
}
.cookiesms{
	width:100%;
	height:43px;
	margin:0 auto;
	padding-left:1%;
        padding-top:5px;
        font-size: 1.2em;
	clear:both;
        font-weight: strong;
color: #333;
bottom:0px;
position:fixed;
left: 0px;
background-color: #FFF;
opacity:0.7;
filter:alpha(opacity=70); /* For IE8 and earlier */
transition: bottom 1s;
-webkit-transition:bottom 1s; /* Safari */
-webkit-box-shadow: 3px -3px 1px rgba(50, 50, 50, 0.56);
-moz-box-shadow:    3px -3px 1px rgba(50, 50, 50, 0.56);
box-shadow:         3px -3px 1px rgba(50, 50, 50, 0.56);
z-index:999999999;
}

.cookiesms:hover{
bottom:0px;
}
.cookies2{
background-color: #FFF;
display:inline;
opacity:0.95;
filter:alpha(opacity=95);
position:absolute;
right: 1%;
top:-30px;
font-size:15px;
height: 30px;
padding-left:25px;
padding-right:25px;
-webkit-border-top-right-radius: 15px;
-webkit-border-top-left-radius: 15px;
-moz-border-radius-topright: 15px;
-moz-border-radius-topleft: 15px;
border-top-right-radius: 15px;
border-top-left-radius: 15px;
-webkit-box-shadow: 3px -3px 1px rgba(50, 50, 50, 0.56);
-moz-box-shadow:    3px -3px 1px rgba(50, 50, 50, 0.56);
box-shadow:         3px -3px 1px rgba(50, 50, 50, 0.56);
}

</style>
<div class="cookiesms text-center" id="cookie1">
Esta web utiliza cookies, puedes ver nuestra  <a href="politicas-cockies"> política de cookies, aquí</a>
Si continuas navegando estás aceptándola
<button class="btn btn-primary" style="
    font-size:15px;
    padding:6px 12px;
    border: none;
    font-weight: 800;
    border-radius: 10px;
" onclick="controlcookies()">Aceptar</button>
<div  class="cookies2" onmouseover="document.getElementById('cookie1').style.bottom = '0px';">
    Política de cookies +
</div>

</div>
<script type="text/javascript">
if (localStorage.controlcookie>0){
document.getElementById('cookie1').style.bottom = '-50px';
}
</script>
<!-- Fin de Plitica de cookies --->
@endsection
