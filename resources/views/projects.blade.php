@extends('layouts.app')
@section('content')
<div class="togo-side-content">
    <style>
.projects{
overflow:visible!important;
overflow-x: auto!important;
white-space:nowrap!important;
}
.projects .item{
display: inline-block!important;
}
</style>
<section id="div" class="projects pt-100 pb-100">
    <div  class="container" style="width: 2010px!important;max-width: 2500px!important;">
        <div  class="row mb-4" style="width: 100%;">
            <div class="col">
                <div class="item mb-50 scrollflow -pop -opacity">
                    <img loading="lazy" loading="lazy" src="img/proyectos/animacion-3D.png" alt="Animaciom3D">
                    <div class="con">
                        <h5><a href="proyecto-animacion-3D">Animación 3D y 2D</a></h5>
                        <span class="category">
                            Proyecto
                        </span>
                        
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="item mb-50 scrollflow -pop -opacity" style="width:100%;">
                        <img loading="lazy" src="img/detalle-proyecto/kellerapp2.jpg" alt="AppMovil">
                    <div class="con">
                        <h5><a style="font-size: 23px!important;" href="proyecto-app-movil-keller">Sistema Inteligente Centinela</a></h5>
                        <span class="category">
                            App Movil
                        </span>
                    </div>
                </div>
            </div>
            <!--<div class="col">-->
            <!--    <div class="item mb-50 scrollflow -pop -opacity">-->
            <!--        <img loading="lazy" src="img/proyectos/app-movil.jpg" alt="">-->
            <!--        <div class="con">-->
            <!--            <h5><a href="proyecto-app-orvity">Orvity</a></h5>-->
            <!--            <span class="category">-->
            <!--                app Moviles-->
            <!--            </span>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</div>-->
            <div class="col">
                <div class="item mb-50 scrollflow -pop -opacity">
                        <img loading="lazy" src="img/detalle-proyecto/sustent4.jpg" alt="PaginaWeb">
                    <div class="con">
                        <h5><a href="proyecto-pagina-web-sustenthabith">Sustenthabit</a></h5>
                        <span class="category">
                            Pagina Web
                        </span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="item mb-50 scrollflow -pop -opacity">
                        <img loading="lazy" src="img/proyectos/pagina-web.png" alt="SistemaWeb">
                    <div class="con">
                        <h5><a href="proyecto-sistema-web-lumi">LUMI</a></h5>
                        <span class="category">
                            Plataforma Web
                        </span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="item mb-50 scrollflow -pop -opacity">
                        <img loading="lazy" src="img/detalle-proyecto/spotp.jpeg" alt="SpotPublicitario">
                    <div class="con">
                        <h5 style="font-size: 23px!important;"><a href="proyecto-sistema-web-keller">Sistema Inteligente Centinela</a></h5>
                        <span class="category">
                            Spot Publicitario
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="width: 100%;">
            <div class="col">
                <div class="item mb-50 scrollflow -pop -opacity" style="width:100%;">
                        <img loading="lazy" src="img/proyectos/realidad_aumentada.png" alt="RealidadAumentada">
                    <div class="con">
                        <h5><a href="proyecto-realidad-aumentada-vacart">VACART</a></h5>
                        <span class="category">
                            Realidad Aumentada
                        </span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="item mb-50 scrollflow -pop -opacity" style="width:100%;">
                        <img loading="lazy" src="img/proyectos/recorrido-virtual.png" alt="RecorridoVirtual">
                    <div class="con">
                        <h5><a href="proyecto-recorrido-virtual-petrolab">PETROADLAB</a></h5>
                        <span class="category">
                            Recorrido 3D
                        </span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="item mb-50 scrollflow -pop -opacity" style="width:100%;">
                        <img loading="lazy" src="img/detalle-proyecto/tem2.jpg" alt="OnePage">
                    <div class="con">
                        <h5><a href="proyecto-erp-odonto">TEM</a></h5>
                        <span class="category">
                            One page
                        </span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="item mb-50 scrollflow -pop -opacity">
                    <img loading="lazy" src="img/detalle-proyecto/mkt2.jpg" alt="MarketingDigital">
                    <div class="con">
                        <h5><a href="proyecto-marketing-digital">Marketing Digital</a></h5>
                        <span class="category">
                            Marketing Digital
                        </span>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="item mb-50 scrollflow -pop -opacity" style="width:100%;">
                        <img loading="lazy" src="img/detalle-proyecto/holox2.jpg" alt="BlogWeb">
                    <div class="con">
                        <h5><a href="proyecto-pagina-web-holox">Holox-Guru</a></h5>
                        <span class="category">
                            Blog
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
    (function() {
    function scrollHorizontally(e) {
        e = window.event || e;
        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
        document.getElementById('div').scrollLeft -= (delta*40);
        e.preventDefault();
    }
    if (document.getElementById('div').addEventListener) {

        document.getElementById('div').addEventListener("mousewheel", scrollHorizontally, false);

        document.getElementById('div').addEventListener("DOMMouseScroll", scrollHorizontally, false);
    } else {

        document.getElementById('div').attachEvent("onmousewheel", scrollHorizontally);
    }
})();
</script>
@endsection