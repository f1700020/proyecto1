@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Services -->
    <section class="services pt-100 pb-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-20 scrollflow -pop -opacity">
                    <h6 class="small-title">NUESTROS</h6>
                    <h4 class="title">SERVICIOS</h4>
                </div>
                <div class="col-md-4 mb-40">
                    <div class="item bg-1">
                        <div class="con scrollflow -pop -opacity">
                            <div class="numb">
                                <img loading="lazy"src="{{asset('img/web.png')}}" class="img" alt="Animatiomx">
                            </div>
                            <h5>SISTEMAS Y PÁGINAS WEB</h5>
                            <p>Creamos y desarrollamos sistemas y sitios web a la medida de cada empresa.</p>
                            <strong></strong>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-40">
                    <div class="item bg-2">
                        <div class="con scrollflow -pop -opacity">
                            <div class="numb">
                                <img loading="lazy"src="{{asset('img/animacion.png')}}" class="img" alt="Animatiomx">
                            </div>
                            <h5>ANIMACIÓN 2D Y 3D</h5>
                            <p>Contamos con un equipo de especialistas dedicados al diseño, modelado y animación de objetos y personajes.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-40">
                    <div class="item bg-3">
                        <div class="con scrollflow -pop -opacity">
                            <div class="numb">
                                <img loading="lazy"src="{{asset('img/apps.png')}}" class="img" alt="Animatiomx">
                            </div>
                            <h5>APLICACIONES MÓVILES</h5>
                            <p>Contamos con un equipo de especialistas dedicados al diseño, modelado y animación de objetos y personajes.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-40">
                    <div class="item bg-4">
                        <div class="con scrollflow -pop -opacity">
                            <div class="numb">
                                <img loading="lazy"src="{{asset('img/reali-aument.png')}}" class="img" alt="Animatiomx">
                            </div>
                            <h5>REALIDAD AUMENTADA</h5>
                            <p>Interacción con dispositivos móviles sobre imágenes u objetos físicos para darle un valor agregado a las marcas.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-40">
                    <div class="item bg-5">
                        <div class="con scrollflow -pop -opacity">
                            <div class="numb">
                                <img loading="lazy"src="{{asset('img/spot.png')}}" class="img" alt="Animatiomx">
                            </div>
                            <h5>SPOTS ANIMADOS</h5>
                            <p>Videos producidos con diferentes técnicas como motion graphics, stop motion o 3D.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-40">
                    <div class="item bg-6">
                        <div class="con scrollflow -pop -opacity">
                            <div class="numb">
                                <img loading="lazy"src="{{asset('img/reco-3d.png')}}" class="img" alt="Animatiomx">
                            </div>
                            <h5>RECORRIDO VIRTUAL 3D</h5>
                            <p>Recorridos para conocer de manera interactiva el interior de las instalaciones de un lugar en específico.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-40">
                    <div class="">
                        <div class="con">
                            <div class="numb">
                                <!-- <img loading="lazy"src="{{asset('')}}img/marketing.png" class="img"> -->
                            </div>
                            <!-- <h5>MARKETING DIGITAL</h5>
                            <p>Aplicamos estrategias de comercialización implementándolas en medios digitales para hacer crecer tu marca</p> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-40">
                    <div class="item bg-7">
                        <div class="con scrollflow -pop -opacity">
                            <div class="numb">
                                <img loading="lazy"src="{{asset('img/marketing.png')}}" class="img" alt="Animatiomx">
                            </div>
                            <h5>MARKETING DIGITAL</h5>
                            <p>Aplicamos estrategias de comercialización implementándolas en medios digitales para hacer crecer tu marca</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- Testimonials -->
    <section class="testimonial bg-img bg-fixed pos-re mt-80 pt-100 pb-100" data-overlay-dark="6" style="background: url(public/img/quote.jpg) center;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="testimonials"> <span class="icon"><img loading="lazy"src="{{asset('img/left-quote.png')}}" alt="Animatiomx"></span>
                        <div class="owl-carousel owl-theme text-center">
                            <div class="item">
                                <div class="client-area">
                                    <h6>Emma Brown</h6> <span>Claire Towers, CEO</span>
                                </div>
                                <p>" Vivamus faucibus lorem non mi varius, vel euismod magna malesuada. Nam nec diam in nisl porttitor commodo in et ligula. "</p>
                            </div>
                            <div class="item">
                                <div class="client-area">
                                    <h6>Jesica White</h6> <span>Drana Inc, Manager</span>
                                </div>
                                <p>" Qaulity faucibus lorem non mi varius, vel euismod magna malesuada. Nam nec diam in nisl porttitor commodo in et ligula. "</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection