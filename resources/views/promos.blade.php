@extends('layouts.app') @section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Post -->
    <section class="post contact pb-100">
        <div class="container" style="
                background-image:url({{asset('img/1920x1280.jpg')}});
                background-position: center;
                background-size: cover;
                background-repeat: no-repeat;">
            <div class="row">
                <!-- Content -->
                <div class="col-md-7 my-auto xs-mt">
                    <h5 style="font-size: 16px;" class="text-right xs-center scrollflow -slide-top -opacity">TRANSFORMA TU NEGOCIO</h5>
                    <h4 class="text-right xs-center scrollflow -slide-top -opacity" style="color: #F3CC23;">DIFERÉNCIATE</h4>
                    <h6 style="font-size: 14px;" class="text-right xs-center scrollflow -slide-top -opacity">CONTRATA TU PRESENCIA WEB</h6>
                </div>
                <div class="col-md-5 py-5">
                    <form method="POST" action="{{url('contacto')}}" class="form scrollflow -slide-bottom -opacity">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('name')}}" id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                                        {{$errors->first('name')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('email')}}" id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        {{$errors->first('email')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('phone')}}" id="form_phone" type="text" name="phone" maxlength="10" placeholder="Teléfono" required="required" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        {{$errors->first('phone')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('asunto')}}" id="form_asunto" type="text" name="asunto" placeholder="Asunto" required="required">
                                        {{$errors->first('asunto')}}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select name="subject" id="form_subject" style="color:white;">
                                            <option value="{{old('subject')}}">Selecciona un servicio</option>
                                            <option value="SISTEMAS Y PÁGINAS WEB">
                                                SISTEMAS Y PAGINAS WEB
                                            </option>
                                            <option value="ANIMACIÓN 2D Y 3D">
                                                ANIMACIÓN 2D Y 3D
                                            </option>
                                            <option value="APLICACIONES MÓVILES">
                                                APLICACIONES MOVILES
                                            </option>
                                            <option value="REALIDAD AUMENTADA">
                                                REALIDAD AUMENTADA
                                            </option>
                                            <option value="SPOTS ANIMADOS">
                                                SPOTS ANIMADOS
                                            </option>
                                            <option value="RECORRIDO VIRTUAL 3D">
                                                RECORRIDO VIRTUAL 3D
                                            </option>
                                            <option value="MARKETING DIGITAL">
                                                MARKETING DIGITAL
                                            </option>
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea value="{{old('message')}}" id="form_message" name="message" placeholder="Message" rows="4" required="required"></textarea>
                                        {{$errors->first('message')}}
                                    </div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn"><span>ENVIAR<i class="ti-arrow-right"></i></span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @if(session()->has('alert-success'))
                    <div class="alert alert-success">
                        {{ session()->get('alert-success') }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="container mt-3" style="position: relative;">
            <h6 class="small-title text-center scrollflow -pop -opacity">
                DISEÑAMOS TU MEJOR PRESENCIA WEB
            </h6>
            <h4 class="title text-center scrollflow -pop -opacity" style="background: none;">
                SOLUCIONES DE ACUERDO A TUS OBJETIVOS
            </h4>

            <a target="_blank" id="xs-whats" href="https://wa.me/528132806645?text=Me%20gustaría%20solicitar%20una%20cotización" style="position:absolute;right:20px;top:5px;"><img loading="lazy" loading="lazy" loading="lazy" loading="lazy" src="{{asset('img/icono-whatsapp.png')}}" alt="Animatiomx" class="img"></a>
            <h4 class="small-title text-center" style="background: none;">
                ¿QUÉ NECESITO?
            </h4>

            <nav id="tabs" class="px-5 scrollflow -slide-top -opacity">
                <div class="nav nav-tabs nav-fill mb-3" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                        LANDING PAGE</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                        ONE PAGE</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile2" role="tab" aria-controls="nav-profile" aria-selected="false">
                        SITIO WEB</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile3" role="tab" aria-controls="nav-profile" aria-selected="false">
                        BLOG</a>
                </div>
            </nav>
            <div class="tab-content px-5 mb100" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row m-0 py-2 scrollflow -slide-top -opacity">
                        <div class="col-md-5">
                            <img loading="lazy" loading="lazy" loading="lazy" loading="lazy" src="{{asset('img/sitio_enbox.png')}}" alt="Animatiomx" />
                        </div>
                        <div class="col-md-7">
                            <div class="row my-3">
                                <h6 class="xs-just">Una Landing Page (página de aterrizaje), desarrollada con el único objetivo de convertir los visitantes en Leads o prospectos de ventas por medio de una oferta determinada. Generalmente tiene un diseño más sencillo con pocos enlaces e informaciones básicas sobre la oferta, además de un formulario para realizar la conversión.</h6>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h6 style="color: #F3CC23;">CARACTERÍSTICAS</h6>
                                    <ul>
                                        <li>·No tiene navegación convencional</li>
                                        <li>·Utiliza un titular corto, directo y sencillo</li>
                                        <li>·Elimina el menú principal y minimiza el pie de pagina</li>
                                        <li>·Formulario de datos</li>
                                    </ul>
                                </div>
                                <div class="col-sm-6">
                                    <h6 style="color: #F3CC23;">USOS</h6>
                                    <ul>
                                        <li>·Recabar información </li>
                                        <li>·Promociones</li>
                                        <li>·Ofertas Especiales</li>
                                        <li>·Campañas de Google Adwords</li>
                                        <li>·Recabar información</li>
                                        <li>·Donaciones</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="row m-0 py-2 scrollflow -slide-top -opacity">
                        <div class="col-md-5">
                            <img loading="lazy" loading="lazy" loading="lazy" loading="lazy" src="{{asset('img/sitio_jabones.png')}}" />
                        </div>
                        <div class="col-md-7">
                            <div class="row mb-4">
                                <h6 class="xs-just">Este tipo de sitios web donde se puede comunicar un mensaje claro y simple, con datos concisos, es clave para que el cliente potencial no tenga que invertir mucho tiempo en encontrar la información que necesita y realizar la acción que nosotros deseamos, una compra, la contratación de un servicio.</h6>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h6 style="color: #F3CC23;">CARACTERÍSTICAS</h6>
                                    <ul>
                                        <li>·Uso Intuitivo</li>
                                        <li>·Simplificación de información.</li>
                                        <li>·Carga rápida, sitio ligero.</li>
                                        <li>·Organización de contenido</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <h6 style="color: #F3CC23;">USOS</h6>
                                    <ul>
                                        <li>·Campaña publicitaria de internet.</li>
                                        <li>·Comunicación institucional.</li>
                                        <li>·Emprendimiento.</li>
                                        <li>·Software y/o apps</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="nav-profile2" role="tabpanel" aria-labelledby="nav-profile2-tab">
                    <div class="row m-0 py-2 scrollflow -slide-top -opacity">
                        <div class="col-md-5">
                            <img loading="lazy" loading="lazy" loading="lazy" loading="lazy" src="{{asset('img/sitio_siprom.png')}}" alt="Animatiomx" />
                        </div>
                        <div class="col-md-7">
                            <div class="row mb-4">
                                <h6 class="xs-just">Un sitio web, se trata de un conjunto de páginas web que son accesibles desde un mismo dominio o subdominio, que son utilizados por negocios de diversa índole para dar a conocer sus productos o sus servicios.  incluyen documentos, fotografías, sonidos, vídeos, animaciones y otro tipo de contenidos que pueden compartirse en línea</h6>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h6 style="color: #F3CC23;">CARACTERÍSTICAS</h6>
                                    <ul>
                                        <li>·Visibilidad</li>
                                        <li>·Genera imagen y amplitud de mercado</li>
                                        <li>·Fácil de actualizar</li>
                                        <li>·Ventaja competitiva</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <h6 style="color: #F3CC23;">USOS</h6>
                                    <ul>
                                        <li>·Venta de productos o servicios</li>
                                        <li>·Mercadotecnia</li>
                                        <li>·Soporte a clientes</li>
                                        <li>·Reclutamiento de Personal</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="nav-profile3" role="tabpanel" aria-labelledby="nav-profile3-tab">
                    <div class="row m-0 py-2 scrollflow -slide-top -opacity">
                        <div class="col-md-5">
                            <img loading="lazy" loading="lazy" loading="lazy" loading="lazy" src="{{asset('img/sitio_tem.png')}}" alt="Animatiomx" />
                        </div>
                        <div class="col-md-7">
                            <div class="row mb-4">
                                <h6 class="xs-just">Un blog es una herramienta que puede ser muy útil para alcanzar objetivos corporativos y entablar un diálogo con la audiencia.</h6>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h6 style="color: #F3CC23;">CARACTERÍSTICAS</h6>
                                    <ul>
                                        <li>·Referente de contenido de calidad</li>
                                        <li>·Generar confianza</li>
                                        <li>·Atraer tráfico</li>
                                        <li>·Generar canales de comunicación interna</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <h6 style="color: #F3CC23;">USOS</h6>
                                    <ul>
                                        <li>·Compartir contenido</li>
                                        <li>·Almacenar información</li>
                                        <li>·Publicar artículos</li>
                                        <li>·Información complementaria de los servicios o productos de la empresa</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h6 class="small-title text-center mt-5 scrollflow -slide-top -opacity">
                CONOCE LAS
            </h6>
            <h4 class="title text-center scrollflow -slide-top -opacity" style="background: none;">
                PROMOCIONES VIGENTES
            </h4>
            <div class="container" style="position: relative;">

                <a target="_blank" class="btn btn-primary btns" style="background: transparent;width:13%;border: none;position: absolute;top: 34%;right: 61%;height:15%;" href="https://wa.me/528132806645?text=Me%20intereza%20la%20promoción%20de%20LandingPage"></a>

                <a target="_blank" class="btn btn-primary btns" style="background: transparent;width:13%;border: none;position: absolute;top: 34%;right: 44%;height:15%;" href="https://wa.me/528132806645?text=Me%20intereza%20la%20promoción%20de%20OnePage"></a>

                <a target="_blank" class="btn btn-primary btns" style="background: transparent;width:13%;border: none;position: absolute;top: 34%;right: 26%;height:15%;" href="https://wa.me/528132806645?text=Me%20intereza%20la%20promoción%20de%20SitioWeb"></a>

                <a target="_blank" class="btn btn-primary btns" style="background: transparent;width:13%;border: none;position: absolute;top: 34%;right: 7%;height:15%;" href="https://wa.me/528132806645?text=Me%20intereza%20la%20promoción%20de%20Blog"></a>

                <img loading="lazy" loading="lazy" loading="lazy" loading="lazy" src="{{asset('img/Captura.png')}}" alt="Animatiomx">
            </div>
            <div class="col-md-6 offset-md-3 mt-5">
                <div class="col-md-12 mb-20 text-center">
                    <h6 class="small-title scrollflow -slide-top -opacity">ESCRÍBENOS Y</h6>
                    <h4 class="title scrollflow -slide-top -opacity">CONTRATA TU PRESENCIA WEB</h4>
                </div>
                <form class="form scrollflow -slide-top -opacity" id="contact-form">
                    <div class="messages"></div>
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group has-error has-danger">
                                    <input id="form_name" type="text" name="name" placeholder="Nombre" required="required"> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-error has-danger">
                                    <input id="form_email" type="email" name="email" placeholder="Email" required="required"> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-error has-danger">
                                    <input id="form_phone" type="text" name="phone" placeholder="Teléfono" required="required"> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-error has-danger">
                                    <input id="form_asunto" type="email" name="asunto" placeholder="Asunto" required="required"> </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select name="subject" id="form_subject" style="color:white;">
                                        <option value="0">Selecciona un servicio</option>
                                        <option value="1">SISTEMAS Y PÁGINAS WEB</option>
                                        <option value="2">ANIMACIÓN 2D Y 3D</option>
                                        <option value="3">APLICACIONES MÓVILES</option>
                                        <option value="4">REALIDAD AUMENTADA</option>
                                        <option value="5">SPOTS ANIMADOS</option>
                                        <option value="6">RECORRIDO VIRTUAL 3D</option>
                                        <option value="7">MARKETING DIGITAL</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea id="form_message" name="message" placeholder="Message" rows="4" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn"><span>ENVIAR<i class="ti-arrow-right"></i></span></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@endsection