@extends('layouts.app')
@section('content')
<!-- Content -->
<div class="togo-side-content">
    <!-- Contact -->
    <section class="contact mt-80 pb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-20 scrollflow -slide-bottom -opacity">
                    <h6 class="small-title xs-center">¿NECESITAS AYUDA?</h6>
                    <h4 class="title xs-center">CONTÁCTANOS</h4>
                    <p>UBICACIÓN DE NUESTRAS SUCURSALES</p>
                    <p>También puede localizarnos en:</p>
                </div>
                <div class="col-md-6">
                    <div class="item bg-contact">
                        <div class="con scrollflow -slide-bottom -opacity">
                            <h5 class="xs-center">PUEBLA</h5>
                            <p><i class="ti-home" style="font-size: 15px; color: #c5a47e;"></i> Av. 14 Ote 604-6 San Andrés Cholula, Puebla</p>
                            <p><i class="ti-mobile" style="font-size: 15px; color: #c5a47e;"></i> (222) 178 25 65</p>
                            <p><i class="ti-envelope" style="font-size: 15px; color: #c5a47e;"></i>
                            contacto@animatiomx.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="item bg-contact2">
                        <div class="con scrollflow -slide-bottom -opacity">
                            <h5 class="xs-center">MONTERREY</h5>
                            <p><i class="ti-home" style="font-size: 15px; color: #c5a47e;"></i> Torre C IOS Campestre Ricardo Margain Zozaya. San Pedro Garza y Garcia, N. L.</p>
                            <p><i class="ti-mobile" style="font-size: 15px; color: #c5a47e;"></i> (818) 000 72 88</p>
                            <p><i class="ti-envelope" style="font-size: 15px; color: #c5a47e;"></i>
                            contacto@animatiomx.com</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-12 text-center scrollflow -slide-bottom -opacity">
                    <h6 class="small-title">¿TIENES DUDAS?</h6>
                    <h4 class="title">ESCRÍBENOS</h4>
                </div>
                <div class="col-md-6 offset-md-3">

                    <form id="Form" method="POST" action="{{url('contacto')}}" class="form scrollflow -slide-bottom -opacity">
                        @csrf
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('name')}}" id="form_name" type="text" name="name" placeholder="Nombre" required="required">
                                        {{$errors->first('name')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('email')}}" id="form_email" type="email" name="email" placeholder="Email" required="required">
                                        {{$errors->first('email')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('phone')}}" id="form_phone" type="text" name="phone"
                                        maxlength="10" placeholder="Teléfono" required="required" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        {{$errors->first('phone')}}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-error has-danger">
                                        <input value="{{old('asunto')}}" id="form_asunto" type="text" name="asunto" placeholder="Asunto" required="required">
                                        {{$errors->first('asunto')}}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" style="color:white;">
                                            <option value="{{old('subject')}}">Selecciona un servicio</option>
                                            <option value="SISTEMAS Y PÁGINAS WEB">
                                                SISTEMAS Y PAGINAS WEB
                                            </option>
                                            <option value="ANIMACIÓN 2D Y 3D">
                                                ANIMACIÓN 2D Y 3D
                                            </option>
                                            <option value="APLICACIONES MÓVILES">
                                                APLICACIONES MOVILES
                                            </option>
                                            <option value="REALIDAD AUMENTADA">
                                                REALIDAD AUMENTADA
                                            </option>
                                            <option value="SPOTS ANIMADOS">
                                                SPOTS ANIMADOS
                                            </option>
                                            <option value="RECORRIDO VIRTUAL 3D">
                                                RECORRIDO VIRTUAL 3D
                                            </option>
                                            <option value="MARKETING DIGITAL">
                                                MARKETING DIGITAL
                                            </option>
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select  name="subject" id="form_subject" style="color:white;">
                                            <option value="{{old('subject')}}">Animación que requiere </option>
                                            <option value="Spots publicitarios 2D stop motions">
                                                Spots publicitarios 2D stop motions 
                                            </option>
                                            <option value="Spots publicitarios 2D motions graphics">
                                                Spots publicitarios 2D motions graphics 
                                            </option>
                                            <option value="Spots publicitarios 3D motion graphics ">
                                                Spots publicitarios 3D motion graphics 
                                            </option>
                                            <option value="Cortometraje 2D Motions grapchis Max 2 mins">
                                                Cortometraje 2D Motions grapchis Max 2 mins 
                                            </option>
                                            <option value="Cortometraje 3D Motions grapchis Máx 2 mins ">
                                                Cortometraje 3D Motions grapchis Máx 2 mins 
                                            </option>
                                            
                                        </select>
                                        {{$errors->first('subject')}}
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea value="{{old('message')}}" id="form_message" name="message" placeholder="Message" rows="4" required="required"></textarea>
                                        {{$errors->first('message')}}
                                    </div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn"><span>ENVIAR<i class="ti-arrow-right"></i></span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @if(session()->has('alert-success'))
    <div class="alert alert-success">
     {{ session()->get('alert-success') }}
    </div>
@endif
                </div>
            </div>
        </div>
    </section>
</div>
@endsection