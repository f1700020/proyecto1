<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/servicios', function () {
    return view('services');
});
Route::get('/contacto', function () {
    return view('contact');
});
Route::post('/contacto', 'MessagesController@store');

Route::get('/nosotros', function () {
    return view('about');
});
Route::get('/proyectos', function () {
    return view('projects');
});
Route::get('/promociones', function () {
    return view('promos');
});
Route::get('/proyecto', function () {
    return view('project');
});
Route::get('/politicas-cockies', function () {
    return view('blog');
});
/*
|--------------------------------------------------------------------------
| Projects Routes
|--------------------------------------------------------------------------
*/
Route::get('/proyecto-animacion-3D', function () {
    return view('projects/project-3d');
});
Route::get('/proyecto-marketing-digital', function () {
    return view('projects/project-mkt');
});
Route::get('/proyecto-erp-odonto', function () {
    return view('projects/project-odonto');
});
Route::get('/proyecto-app-orvity', function () {
    return view('projects/project-orv');
});
Route::get('/proyecto-recorrido-virtual-petrolab', function () {
    return view('projects/project-petrolab');
});
Route::get('/proyecto-pagina-web-sustenthabith', function () {
    return view('projects/project-sust');
});
Route::get('/proyecto-realidad-aumentada-vacart', function () {
    return view('projects/project-vacart');
});
Route::get('/proyecto-pagina-web-holox', function () {
    return view('projects/project-holox');
});
Route::get('/proyecto-app-movil-keller', function () {
    return view('projects/project-keller-app');
});
Route::get('/proyecto-sistema-web-keller', function () {
    return view('projects/project-keller-web');
});
Route::get('/proyecto-sistema-web-lumi', function () {
    return view('projects/project-lumi');
});