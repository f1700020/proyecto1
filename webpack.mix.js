const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    'resources/js/eskju.jquery.scrollflow.min.js',
    'resources/js/jquery-3.0.0.min.js',
    'resources/js/jquery-migrate-3.0.0.min.js',
    'resources/js/popper.min.js',
    'resources/js/bootstrap.min.js',
    'resources/js/jquery.waypoints.min.js',
    'resources/js/owl.carousel.min.js',
    'resources/js/jquery.stellar.min.js',
    'resources/js/YouTubePopUp.jquery.js',
    'resources/js/validator.js',
    'resources/js/autotype.js',
    'resources/js/bootstrap.menu.js',
    'resources/js/scripts.js',
], 'public/js/app.js')

    .styles([
        'resources/sass/bootstrap.min.css',
        'resources/sass/bootstrap.menu.css',
        'resources/sass/animate.min.css',
        'resources/sass/themify-icons.css',
        'resources/sass/YouTubePopUp.css',
        'resources/sass/owl.carousel.min.css',
        'resources/sass/owl.theme.default.min.css',
        'resources/sass/style.css'
    ], 'public/css/app.css');
