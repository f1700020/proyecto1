<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\contacto;

class MessagesController extends Controller
{
  public function store(Request $request)
   {
        $messagecontact =  request()->validate([
            'name'=> 'required',
            'email'=> 'required',
            'phone'=> ['required', 'max:10'],
            'asunto'=> 'required',
            'subject'=> 'required',
            'message'=> 'required'
        ]);

    Mail::to('animatiomx@gmail.com')->send(new contacto($messagecontact));

        return redirect("contacto")->with('alert-success', 'Mensaje enviado correctamente');
   }
}
